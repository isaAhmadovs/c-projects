﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Runtime.Serialization.Formatters.Binary;
using Newtonsoft.Json.Linq;
using SHUSHEN;

namespace SHUSHEN
{

    enum SUBCategory { TRANSPORT = 1, MEAL, SPORT, HOUSE, MEDICINE, PHONE, SHOPPINGONLINE, Shopping, OTHER };


    enum METHOD { CARD, CASH };

    class Stat
    {
        public void Show(SUBCategory s, int balnce, int spend)
        {
            if (balnce == 0)
                balnce = 1;

            DateTime today = DateTime.Now;
            Console.WriteLine("                         " + today);

            if (s == (SUBCategory)1)
            {
                Console.WriteLine("TRANSPORT:");
                Console.WriteLine((spend * 100) / balnce + "%");
            }
            else if (s == (SUBCategory)2)
            {
                Console.WriteLine("MEAL:");
                Double R = (spend * 100) / balnce;
                Console.WriteLine(R + "%");
            }
            else if (s == (SUBCategory)3)
            {
                Console.WriteLine("SPORT:");
                Double R = (spend * 100) / balnce;
                Console.WriteLine(R + "%");
            }
            else if (s == (SUBCategory)4)
            {
                Console.WriteLine("HOUSE:");
                Double R = (spend * 100) / balnce;
                Console.WriteLine(R + "%");
            }
            else if (s == (SUBCategory)5)
            {
                Console.WriteLine("MEDICINE:");
                Double R = (spend * 100) / balnce;
                Console.WriteLine(R + "%");
            }
            else if (s == (SUBCategory)6)
            {
                Console.WriteLine("PHONE:");
                Double R = (spend * 100) / balnce;
                Console.WriteLine(R + "%"); ;
            }
            else if (s == (SUBCategory)7)
            {
                Console.WriteLine("Shopping Online:");
                Double R = (spend * 100) / balnce;
                Console.WriteLine(R + "%");
            }
            else if (s == (SUBCategory)8)
            {
                Console.WriteLine("Shopping:");
                Double R = (spend * 100) / balnce;
                Console.WriteLine(R + "%");
            }

            else if (s == (SUBCategory)9)
            {
                Console.WriteLine("Other:");
                Double R = (spend * 100) / balnce;
                Console.WriteLine(R + "%");
            }
        }



        public void Shownotes(SUBCategory s, List<string> l, int amount)
        {
            if ((int)s == 1)
            {

                Console.WriteLine("Transport:");
                foreach (var item in l)
                {
                    Console.WriteLine("Amount spent:" + amount);
                    Console.WriteLine(item);
                    Console.WriteLine();
                }
                Console.WriteLine();

            }
            else if ((int)s == 2)
            {
                Console.WriteLine("Meal:");
                foreach (var item in l)
                {
                    Console.WriteLine("Amount spent:" + amount);
                    Console.WriteLine(item);
                    Console.WriteLine();
                }
                Console.WriteLine();

            }

            else if ((int)s == 3)
            {
                Console.WriteLine("SPORT:");
                foreach (var item in l)
                {
                    Console.WriteLine("Amount spent:" + amount);
                    Console.WriteLine(item);
                    Console.WriteLine(item + " " + amount);
                }
                Console.WriteLine();

            }
            else if ((int)s == 4)
            {
                Console.WriteLine("HOUSE:");
                foreach (var item in l)
                {
                    Console.WriteLine("Amount spent:" + amount);
                    Console.WriteLine(item);
                    Console.WriteLine(item + " " + amount);
                }
                Console.WriteLine();

            }
            else if ((int)s == 5)
            {
                Console.WriteLine("MEDICINE:");
                foreach (var item in l)
                {
                    Console.WriteLine("Amount spent:" + amount);
                    Console.WriteLine(item);
                    Console.WriteLine(item + " " + amount);
                }
                Console.WriteLine();

            }
            else if ((int)s == 6)
            {
                Console.WriteLine("PHONE :");
                foreach (var item in l)
                {
                    Console.WriteLine("Amount spent:" + amount);
                    Console.WriteLine(item);
                    Console.WriteLine(item + " " + amount);
                }
                Console.WriteLine();

            }
            else if ((int)s == 7)
            {
                Console.WriteLine("SHOPPING ONLINE:");
                foreach (var item in l)
                {
                    Console.WriteLine("Amount spent:" + amount);
                    Console.WriteLine(item);
                    Console.WriteLine(item + " " + amount);
                }
                Console.WriteLine();

            }
            else if ((int)s == 8)
            {
                Console.WriteLine("Shopping:");
                foreach (var item in l)
                {
                    Console.WriteLine("Amount spent:" + amount);
                    Console.WriteLine(item);
                    Console.WriteLine(item + " " + amount);
                }
                Console.WriteLine();

            }
            else
            {
                Console.WriteLine("Other:");
                foreach (var item in l)
                {
                    Console.WriteLine("Amount spent:" + amount);
                    Console.WriteLine(item);
                }
                Console.WriteLine();
            }



        }
    }
    [Serializable]
    class Account
    {
        private string name;
        private int balance;
        METHOD method;
        bool hiden;


        public int amountspentontransport;
        public int amountspentonmeal;
        public int amountspentonsport;
        public int amountspentonhouse;
        public int amountspentonmedicine;
        public int amountspentonphone;
        public int amountspentononlineshopping;
        public int ammountspentonshopping;
        public int amountspentonother;

        public int balancetransport = 1;
        public int balancemeal = 1;
        public int balancesport = 1;
        public int balancehouse = 1;
        public int balancemedicine = 1;
        public int balancephone = 1;
        public int balanceonlineshoppping = 1;
        public int balanceshopping = 1;
        public int balanceother = 1;

        public List<string> notesoftransport;
        public List<string> notesofmeal;
        public List<string> notesofsport;
        public List<string> notesofhouse;
        public List<string> notesofmedicine;
        public List<string> notesofphone;
        public List<string> notesofonlineshopping;
        public List<string> notesofshopping;
        public List<string> notesofother;

        public int amountadded = 0;
        public int oldbal;


        public string Name { get => name; set => name = value; }
        public int Balance { get => balance; set => balance = value; }
        public bool Hiden { get => hiden; set => hiden = value; }
        METHOD Method { get => method; set => method = value; }

        public int Amountspentontransport { get => amountspentontransport; set => amountspentontransport = value; }
        public int Amountspentonmeal { get => amountspentonmeal; set => amountspentonmeal = value; }
        public int Amountspentonsport { get => amountspentonsport; set => amountspentonsport = value; }
        public int Amountspentonhouse { get => amountspentonhouse; set => amountspentonhouse = value; }
        public int Amountspentonmedicine { get => amountspentonmedicine; set => amountspentonmedicine = value; }
        public int Amountspentonphone { get => amountspentonphone; set => amountspentonphone = value; }
        public int Amountspentononlineshopping { get => amountspentononlineshopping; set => amountspentononlineshopping = value; }
        public int Ammountspentonshopping { get => ammountspentonshopping; set => ammountspentonshopping = value; }

        public int Balancetransport { get => balancetransport; set => balancetransport = value; }
        public int Balancemeal { get => balancemeal; set => balancemeal = value; }
        public int Balancesport { get => balancesport; set => balancesport = value; }
        public int Balancehouse { get => balancehouse; set => balancehouse = value; }
        public int Balancemedicine { get => balancemedicine; set => balancemedicine = value; }
        public int Balancephone { get => balancephone; set => balancephone = value; }
        public int Balanceonlineshoppping { get => balanceonlineshoppping; set => balanceonlineshoppping = value; }
        public int Balanceshopping { get => balanceshopping; set => balanceshopping = value; }
        public int Amountspentonother { get => amountspentonother; set => amountspentonother = value; }
        public int Balanceother { get => balanceother; set => balanceother = value; }
        public int Oldbal { get => oldbal; set => oldbal = value; }

        public Account(string name = null, int balance = 0, METHOD method = (METHOD)1, bool hiden = false)
        {
            Name = name;
            Balance = balance;
            Method = method;
            Hiden = hiden;
            notesoftransport = new List<string> { " " };
            notesofsport = new List<string> { " " };
            notesofshopping = new List<string> { " " };
            notesofphone = new List<string> { " " };
            notesofother = new List<string> { " " };
            notesofonlineshopping = new List<string> { " " };
            notesofmedicine = new List<string> { " " };
            notesofmeal = new List<string> { " " };
            notesofhouse = new List<string> { " " };
            oldbal = balance;
        }


        public override string ToString()
        {

            DateTime today = DateTime.Now;
            Console.WriteLine("                         " + today);
            Console.WriteLine(name);
            if (hiden == true)
            {
                Console.WriteLine(" Hidden");
            }
            else
            {
                Console.WriteLine("Not Hidden");
            }
            if (method == (METHOD)0)
            {
                Console.Write("Method: ");
                Console.WriteLine("Card");
            }
            else if (method == (METHOD)1)
            {
                Console.Write("Method: ");
                Console.WriteLine("Cash");
            }
            WebClient wc = new WebClient();
            try
            {
                string url = "http://www.apilayer.net/api/live?access_key=8d8c5887112d85a783fcdd84c44ab6eb";

                var r = wc.DownloadString(url);
                dynamic f = JObject.Parse(r);
                Console.WriteLine("                  Balance in USD:" + balance);
                Console.WriteLine("                  Balance in AZN:" + balance * (float)f.quotes.USDAZN);
                Console.WriteLine("                  Balance in EUR:" + balance * (float)f.quotes.USDEUR);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Check your internet connection");
                Console.WriteLine("Do you want to use our course?");
                Console.WriteLine("1)Yes,please");
                Console.WriteLine("2)No,i will fix my internet connection.");
                char t = Convert.ToChar(Console.ReadLine());
                if (t == '2')
                {
                    ToString();
                }
                else if (t == '1') {
                    Console.WriteLine("                  Balance in USD:" + balance);
                    Console.WriteLine("                  Balance in AZN:" + balance * 1.7);
                    Console.WriteLine("                  Balance in EUR:" + balance * 0.82);
                }
               
            }
            return " ";

        }

        public void AddBalance()
        {
            Console.WriteLine("Enter amount:");
            int tmp = Int32.Parse(Console.ReadLine());
            amountadded = +tmp;
            balance += tmp;
            oldbal += tmp;
        }


        public void SubBalance()
        {


            Console.WriteLine("Enter amount in USD:");
            int tmp = Int32.Parse(Console.ReadLine());
            balance -= tmp;
            Console.WriteLine("Choose category:");
            Console.WriteLine("1)Transport");
            Console.WriteLine("2)Meal");
            Console.WriteLine("3)Sport");
            Console.WriteLine("4)House");
            Console.WriteLine("5)Medicine");
            Console.WriteLine("6)Phone");
            Console.WriteLine("7)Shopping Online");
            Console.WriteLine("8)Shopping");
            Console.WriteLine("9)Other");
            Char cate = Char.Parse(Console.ReadLine());
            if (cate == '1')
            {
                balancetransport = Oldbal;
                Console.WriteLine("Enter note:");
                string s = Console.ReadLine();
                notesoftransport.Add(s);
                Console.WriteLine("Succes");
                Amountspentontransport += tmp;
            }
            else if (cate == '2')
            {
                Balancemeal = Oldbal;
                Console.WriteLine("Enter note:");
                string s = Console.ReadLine();
                notesofmeal.Add(s);

                Console.WriteLine("Succes");

                Amountspentonmeal += tmp;
            }
            else if (cate == '3')
            {
                balancesport = Oldbal;
                Console.WriteLine("Enter note:");
                string s = Console.ReadLine();
                Console.WriteLine("Succes");
                notesofsport.Add(s);
                amountspentonsport += tmp;
            }
            else if (cate == '4')
            {
                Balancehouse = Oldbal;
                Console.WriteLine("Enter note:");
                string s = Console.ReadLine();
                notesofhouse.Add(s);
                Console.WriteLine("Succes");

                Amountspentonhouse += tmp;
            }
            else if (cate == '5')
            {
                Balancemedicine = Oldbal;
                Console.WriteLine("Enter note:");
                string s = Console.ReadLine();
                notesofmedicine.Add(s);
                Console.WriteLine("Succes");

                amountspentonmedicine += tmp;
            }
            else if (cate == '6')
            {
                Balancephone = Oldbal;
                Console.WriteLine("Enter note:");
                string s = Console.ReadLine();
                notesofphone.Add(s);
                Console.WriteLine("Succes");

                Amountspentonphone += tmp;
            }
            else if (cate == '7')
            {
                Balanceonlineshoppping = Oldbal;
                Console.WriteLine("Enter note:");
                string s = Console.ReadLine();
                notesofonlineshopping.Add(s);
                Console.WriteLine("Succes");

                Amountspentononlineshopping += tmp;
            }
            else if (cate == '8')
            {

                Balanceshopping = Oldbal;
                Console.WriteLine("Enter note:");
                string s = Console.ReadLine();
                notesofshopping.Add(s);
                Console.WriteLine("Succes");
                Ammountspentonshopping += tmp;
            }
            else if (cate == '9')
            {
                balanceother = Oldbal;
                Console.WriteLine("Enter note");
                string e = Console.ReadLine();
                notesofother.Add(e);
                Console.WriteLine("Succes");
                Amountspentonother += tmp;
            }


        }
    }


    class WorkWithAccount
    {
        public List<Account> l;
        List<Account> L { get => l; set => l = value; }

        public WorkWithAccount()
        {
            l = new List<Account>();
        }

        public void AddAcc()
        {
            Console.Write("Enter name:");
            string name = Console.ReadLine();
            Console.Write("Enter starting balace in USD:");
            int balance = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Choode method:");
            Console.WriteLine("1)Card");
            Console.WriteLine("2)Cash");
            int a = Int32.Parse(Console.ReadLine());
            a--;
            bool hidden;
            Console.WriteLine("Hidden?");
            Console.WriteLine("1)Yes");
            Console.WriteLine("2)No");
            char c = Char.Parse(Console.ReadLine());
            if (c == '1')
            {
                hidden = true;
            }
            else
            {
                hidden = false;
            }
            Account A = new Account(name, balance, (METHOD)a, hidden);
            l.Add(A);
        }
        public void DelAccount(int i)
        {
            l.RemoveAt(i);
        }
        public void Print()
        {
            foreach (var item in l)
                Console.WriteLine(item);
        }
    }


    class MAINMENU
    {

        public void MAINprint(Account r, WorkWithAccount w)
        {


            using (var fs = new FileStream("data.bin", FileMode.OpenOrCreate))
            {
                var bf = new BinaryFormatter();
                bf.Serialize(fs, w.l);
            }



            Stat s = new Stat();
            Console.WriteLine();
            r.ToString();
            Console.WriteLine();


            Console.WriteLine("1)Add money");
            Console.WriteLine("2)Subtract balance");
            Console.WriteLine("3)Show stat");
            Console.WriteLine("4)Add Account");
            Console.WriteLine("5)Print Accounts ");
            Console.WriteLine("6)Delete  Account");
            Console.WriteLine("7)Switch default account");
            Console.WriteLine("8)Overall balance");
            Console.WriteLine("9)Transfer between accounts");
            Console.WriteLine("10)Show notes");
            Console.WriteLine("X)EXIT");
            Console.WriteLine("X-any number except 1-10");
            try
            {
                Int32 a = Int32.Parse(Console.ReadLine());

                if (a == 1)
                {
                    r.AddBalance();
                    r.balancehouse = r.amountadded + r.balancehouse;
                    r.balancemeal = r.amountadded + r.balancemeal;
                    r.balancemedicine = r.amountadded + r.balancemedicine;
                    r.balanceonlineshoppping = r.amountadded + r.balanceonlineshoppping;
                    r.balanceother = r.amountadded + r.balanceother;
                    r.balancephone = r.amountadded + r.balancephone;
                    r.balanceshopping = r.amountadded + r.Balanceshopping;
                    r.balancesport = r.amountadded + r.balancesport;
                    r.balancetransport = r.amountadded + r.balancetransport;
                    using (var fs = new FileStream("data.bin", FileMode.OpenOrCreate))
                    {
                        var bf = new BinaryFormatter();
                        bf.Serialize(fs, w.l);
                    }

                    Console.Clear();
                    MAINprint(r, w);
                }

                else if (a == 2)
                {

                    r.SubBalance();

                    using (var fs = new FileStream("data.bin", FileMode.OpenOrCreate))
                    {
                        var bf = new BinaryFormatter();
                        bf.Serialize(fs, w.l);
                    }
                    MAINprint(r, w);
                }
                else if (a == 3)
                {
                    Console.Clear();
                    s.Show((SUBCategory)1, r.Balancetransport, r.amountspentontransport);
                    s.Show((SUBCategory)2, r.Balancemeal, r.amountspentonmeal);
                    s.Show((SUBCategory)3, r.Balancesport, r.amountspentonsport);
                    s.Show((SUBCategory)4, r.Balancehouse, r.amountspentonhouse);
                    s.Show((SUBCategory)5, r.balancemedicine, r.amountspentonmedicine);
                    s.Show((SUBCategory)6, r.Balancephone, r.amountspentonphone);
                    s.Show((SUBCategory)7, r.Balanceonlineshoppping, r.amountspentononlineshopping);
                    s.Show((SUBCategory)8, r.Balanceshopping, r.Ammountspentonshopping);
                    s.Show((SUBCategory)9, r.Balanceother, r.Amountspentonother);
                    using (var fs = new FileStream("data.bin", FileMode.OpenOrCreate))
                    {
                        var bf = new BinaryFormatter();
                        bf.Serialize(fs, w.l);
                    }
                    MAINprint(r, w);
                }
                else if (a == 4)
                {
                    Console.Clear();
                    w.AddAcc();
                    Console.WriteLine("Account Added");
                    using (var fs = new FileStream("data.bin", FileMode.OpenOrCreate))
                    {
                        var bf = new BinaryFormatter();
                        bf.Serialize(fs, w.l);
                    }
                    MAINprint(r, w);
                }
                else if (a == 5)
                {
                    Console.Clear();
                    w.Print();
                    using (var fs = new FileStream("data.bin", FileMode.OpenOrCreate))
                    {
                        var bf = new BinaryFormatter();
                        bf.Serialize(fs, w.l);
                    }
                    MAINprint(r, w);
                }
                else if (a == 6)
                {
                    Console.Clear();
                    w.Print();
                    Console.WriteLine("Which Account?");
                    Console.Write("Enter index:");
                    int i = Int32.Parse(Console.ReadLine());
                    w.DelAccount(i);
                    using (var fs = new FileStream("data.bin", FileMode.OpenOrCreate))
                    {
                        var bf = new BinaryFormatter();
                        bf.Serialize(fs, w.l);
                    }
                    MAINprint(r, w);
                }
                else if (a == 7)
                {
                    Console.Clear();
                    w.Print();
                    Console.WriteLine("Which Account?");
                    Console.Write("Enter index:");
                    int i = Int32.Parse(Console.ReadLine());
                    using (var fs = new FileStream("data.bin", FileMode.OpenOrCreate))
                    {
                        var bf = new BinaryFormatter();
                        bf.Serialize(fs, w.l);
                    }
                    MAINprint(w.l[i], w);

                }
                else if (a == 8)
                {
                    Console.Clear();
                    int num = 0;
                    int op = 0;
                    while (num != w.l.Count)
                    {
                        if (w.l[num].Hiden == false)
                        {
                            op = +w.l[num].Balance;

                        }
                        else
                        {
                            op = +0;
                        }
                        num++;
                    }
                    using (var fs = new FileStream("data.bin", FileMode.OpenOrCreate))
                    {
                        var bf = new BinaryFormatter();
                        bf.Serialize(fs, w.l);
                    }
                    Console.WriteLine("Overral balance: " + op);
                    MAINprint(r, w);
                }
                else if (a == 9)
                {
                    Console.Clear();
                    w.Print();
                    Console.Write("Enter index (from which): ");
                    int re = Int32.Parse(Console.ReadLine());
                    Console.Write("To which? ");
                    Int32 t = Int32.Parse(Console.ReadLine());
                    if (t == re)
                    {
                        Console.WriteLine("You can not transfer to the same account");
                        using (var fs = new FileStream("data.bin", FileMode.OpenOrCreate))
                        {
                            var bf = new BinaryFormatter();
                            bf.Serialize(fs, w.l);
                        }
                        MAINprint(r, w);
                    }

                    else
                    {
                        Console.WriteLine("Enter amount: ");
                        int am = Int32.Parse(Console.ReadLine());

                        r.balancehouse = am + r.balancehouse;
                        r.balancemeal = am + r.balancemeal;
                        r.balancemedicine = am + r.balancemedicine;
                        r.balanceonlineshoppping = am + r.balanceonlineshoppping;
                        r.balanceother = am + r.balanceother;
                        r.balancephone = am + r.balancephone;
                        r.balanceshopping = am + r.Balanceshopping;
                        r.balancesport = am + r.balancesport;
                        r.balancetransport = am + r.balancetransport;
                        w.l[re].Balance -= am;
                        w.l[t].Balance += am;
                        using (var fs = new FileStream("data.bin", FileMode.OpenOrCreate))
                        {
                            var bf = new BinaryFormatter();
                            bf.Serialize(fs, w.l);
                        }
                        MAINprint(r, w);
                    }
                }

                else if (a == 10)
                {
                    //TRANSPORT = 1, MEAL, SPORT, HOUSE, MEDICINE, PHONE, SHOPPINGONLINE, Shopping,OTHER}
                    Console.Clear();
                    s.Shownotes((SUBCategory)1, r.notesoftransport, r.Amountspentontransport);
                    s.Shownotes((SUBCategory)2, r.notesofmeal, r.amountspentonmeal);
                    s.Shownotes((SUBCategory)3, r.notesofsport, r.Amountspentonsport);
                    s.Shownotes((SUBCategory)4, r.notesofhouse, r.amountspentonhouse);
                    s.Shownotes((SUBCategory)5, r.notesofmedicine, r.Amountspentonmedicine);
                    s.Shownotes((SUBCategory)6, r.notesofphone, r.amountspentonphone);
                    s.Shownotes((SUBCategory)7, r.notesofonlineshopping, r.Ammountspentonshopping);
                    s.Shownotes((SUBCategory)8, r.notesofshopping, r.ammountspentonshopping);
                    s.Shownotes((SUBCategory)9, r.notesofother, r.amountspentonother);
                    using (var fs = new FileStream("data.bin", FileMode.OpenOrCreate))
                    {
                        var bf = new BinaryFormatter();
                        bf.Serialize(fs, w.l);
                    }
                    MAINprint(r, w);


                }
                else
                {
                    using (var fs = new FileStream("data.bin", FileMode.OpenOrCreate))
                    {
                        var bf = new BinaryFormatter();
                        bf.Serialize(fs, w.l);
                    }
                    Environment.Exit(0);

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Try again,something went wrong");
                if (ex.Message == "Input string was not in a correct format.")
                    Console.WriteLine("Please enter a valuable string");
                else if (ex.Message == "Value was either too large or too small for an Int32.")
                    Console.WriteLine("Too large number");
                //else if (ex.Message == "Index was out of range. Must be non-negative and less than the size of the collection/nParameter name: index")
                //    Console.WriteLine("No such kind of account found");


                else
                    Console.WriteLine(ex.Message);

                MAINprint(r, w);
            }
        }
    }


}

class Program
{

    static void Main()
    {
        Console.WriteLine("Hello");
        try
        {
            Console.WriteLine();
            Console.WriteLine("Continue or do a new account?");
            Console.WriteLine("1)Continue with previous accounts");
            Console.WriteLine("2)Do a new account");
            Console.WriteLine("NOTICE: you can not press continue if you didn't make any account before");
            char qwerty = Convert.ToChar(Console.ReadLine());

            if (qwerty == '2')
            {

                MAINMENU m = new MAINMENU();
                var w = new WorkWithAccount();
                w.l.Clear();
                w.AddAcc();
                Console.Clear();
                m.MAINprint(w.l[0], w);
            }
            else
            {
                MAINMENU m = new MAINMENU();
                var w = new WorkWithAccount();
                using (var fs = new FileStream("data.bin", FileMode.OpenOrCreate))
                {
                    var bf2 = new BinaryFormatter();
                    w.l = bf2.Deserialize(fs) as List<Account>;
                }
                m.MAINprint(w.l[0], w);
            }
        }


        catch (Exception ex)
        {
            Console.WriteLine("Try again,something went wrong");

            if (ex.Message == "Attempting to deserialize an empty stream.")
                Console.WriteLine("you can not choose continue if you didn't make any account before");
            else if (ex.Message == "String must be exactly one character long.")
                Console.WriteLine("You have to enter 1 or 2");
            else
                Console.WriteLine(ex.Message);
            Main();
        }

    }
}
